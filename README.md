# Todo List Backend


## Assignment

Build a backend app that exposes a simple HTTP API for a ToDo list  management. 

You can use any language/framework of your preference


The app shall implement:

- CRUD operations

- List of all ToDo items

- List of completed items

- List of deleted items

- All the unit tests for the functionalities listed above

- The project should include a `docker compose` definition 
    - The app can run locally by executing `docker compose up` command or similar

## Implementations

There are 2 implementations of the backend Todo API, in different branches:

- branch `fastapi` uses  [fastapi with python](https://fastapi.tiangolo.com/)
- branch `spring-rest` uses [springboot reactive](https://spring.io/reactive)


Both branches provide corresponding `dockerfile` and `docker-compose` specifications, with `README.md` instructions for building, running the application
